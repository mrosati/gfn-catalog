# Frontend
The frontend app is in /frontend

## Setup Docker Image
`docker build . -t svelte:latest`


## Setup Project
Run from project root:
`docker run --rm -ti -v $(pwd)/frontend:/app svelte:latest npm install`

## Running
Run from project root:

`docker run --rm -ti -p 35729:35729 -p 5000:5000 -v $(pwd)/frontend:/app svelte:latest npm run dev`

