const CONSTS = {
    STORE_STEAM: "Steam",
    STORE_EPIC: "Epic"
}

const cacheManager = {
    loadData: (key) => {
        const jsonData = localStorage.getItem(key, data)
        return JSON.parse(jsonData)
    },
    saveData: (key, data) => {
        const jsonData = Json.stringify(data);
        localStorage.setItem(key, jsonData)
    }
}

const imageDB = (() => {
    const PLACEHOLDER_IMG = "https://bulma.io/images/placeholders/1280x960.png"


    return {
        getSteamAppImage: (appID) => {
            return "https://cdn.akamai.steamstatic.com/steam/apps/%APPID%/header.jpg?t=1616181628".replace("%APPID%", appID)
        },
        getEpicAppImage: (appID) => {
            return PLACEHOLDER_IMG
        },
        PLACEHOLDER: PLACEHOLDER_IMG
    }
})()




export {
    cacheManager,
    imageDB,
    CONSTS
};